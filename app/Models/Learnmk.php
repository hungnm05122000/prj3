<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Learnmk extends Model
{
    use HasFactory;

    protected $fillable = [
        'point',
        'learnag_id',
        'confirm_flag',
    ];

    public function learnag()
    {
        return $this->belongsTo(Learnag::class, 'learnag_id', 'id');
    }
}
