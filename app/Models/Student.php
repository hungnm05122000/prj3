<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'class_id',
        'sex',
        'phone',
    ];

    public function classrm()
    {
        return $this->belongsTo(Classrm::class, 'class_id', 'id');
    }

    public function getGioiTinhAttribute(): string
    {
        if ($this->sex == 1) {
            return "Nam";
        }

        return "Nữ";
    }
}
