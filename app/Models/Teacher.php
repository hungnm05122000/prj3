<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Teacher extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'confirm_flag',
    ];

    public function getCapDoAttribute(): string // hàm biến đổi thành tên giới tính //
    {
        if ($this->confirm_flag == 1) {
            return "Super Admin";
        }

        return "Admin";
    }
}
