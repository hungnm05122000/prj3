<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subjects extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'major_id',
    ];

    public function majors()
    {
        return $this->belongsTo(Majors::class, 'major_id', 'id');
    }
}
