<?php

namespace App\Http\Controllers;

use App\Models\Majors;
use Illuminate\Http\Request;

class MajorsController extends Controller
{
    public function index()
    {
        $majorss = Majors::orderBy('id', 'desc')->paginate(20)->withQueryString();
        return view('majors.index', compact('majorss'));
    }


    public function store(Request $request)
    {
        Majors::create($request->all());
        return redirect('/majors')->with('success', 'Thêm ngành học thành công!');
    }

    public function show(Majors $majors)
    {
        //
    }

    public function edit($id)
    {
        $majors = Majors::find($id);
        return view('majors.edit', compact('majors'));
    }

    public function update(Request $request)
    {
        $majors = Majors::find($request->id);
        $majors->update($request->all());
        return redirect('/majors')->with('success', 'Cập nhật thành công!');
    }

    public function destroy(Majors $majors)
    {
        //
    }
}
