<?php

namespace App\Http\Controllers;

use App\Models\Learnag;
use App\Models\Learnmk;
use Illuminate\Http\Request;

class LearnmkController extends Controller
{
    public function index($id)
    {
        $learnag = Learnag::find($id);
        $learnmks = Learnmk::where('learnag_id', $id)->get();
        return view('learnmk.index', compact('learnmks', 'learnag'));
    }

    public function store(Request $request)
    {
        $learnmk = new Learnmk();
        $learnag = Learnag::find($request->id);
        if ($request->point > 5) {
            if ($request->confirm_flag == 1) {
                $learnag->status = 1;
            } else {
                $learnag->status = 0;
            }
        } else {
            $learnag->status = 0;
        }
        $learnmk->point = $request->point;
        $learnmk->learnag_id = $request->id;
        $learnmk->confirm_flag = $request->confirm_flag;
        $learnmk->save();
        $learnag->update();
        return redirect()->route('learnag-show', $learnag->class_id)->with('success', 'Cập nhật thành công!');
    }
}
