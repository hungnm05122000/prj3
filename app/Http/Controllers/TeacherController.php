<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class TeacherController extends Controller
{

    public function index()
    {
        if (Auth::guard('teacher')->user()->confirm_flag == 1) {
            $teachers = Teacher::orderBy('id', 'desc')->paginate(20)->withQueryString();
            return view('teacher.index', compact('teachers'));
        } else {
            return redirect('/');
        }
    }


    public function create()
    {
        if (Auth::guard('teacher')->user()->confirm_flag == 1) {
            return view('teacher.create');
        } else {
            return redirect('/');
        }
    }


    public function store(RegisterRequest $request)
    {
        if (Auth::guard('teacher')->user()->confirm_flag == 1) {
            $teacher = new Teacher();
            $teacher->name = $request->name;
            $teacher->email = $request->email;
            $teacher->password = Hash::make($request->password);
            $teacher->phone = $request->phone;
            $teacher->confirm_flag = $request->confirm_flag;
            $teacher->save();
            return redirect('/teacher')->with('success', 'Thêm tài khoản thành công!');
        } else {
            return redirect('/');
        }
    }


    public function show(Teacher $teacher)
    {
        //
    }


    public function edit($id)
    {
        if (Auth::guard('teacher')->user()->confirm_flag == 1) {
            $teacher = Teacher::find($id);
            return view('teacher.edit', compact('teacher'));
        } else {
            return redirect('/');
        }
    }

    public function update(Request $request)
    {
        if (Auth::guard('teacher')->user()->confirm_flag == 1) {
            $teacher = Teacher::find($request->id);
            if ($request->password) {
                $validator = Validator::make($request->all(), [
                    'password' => 'required|min:8',
                ]);
                if ($validator->fails()) {
                    return redirect()->back()->with('error', 'Mật khẩu tối thiểu 8 ký tự!');
                }
                $teacher->password = Hash::make($request->password);
            }
            $teacher->name = $request->name;
            $teacher->phone = $request->phone;
            $teacher->update();
            return redirect('/teacher')->with('success', 'Cập nhật thành công!');
        } else {
            return redirect('/');
        }
    }

    public function destroy(Request $request)
    {
        if (Auth::guard('teacher')->user()->confirm_flag == 1) {
            $teacher = Teacher::find($request->id);
            if ($teacher->confirm_flag == 1) {
                return redirect()->back()->with('error', 'Tài khoản này không được xóa!');
            }
            $teacher->delete();
            return redirect('/teacher')->with('success', 'Xóa tài khoản thành công!');
        } else {
            return redirect('/');
        }
    }
}
