<?php

namespace App\Http\Controllers;

use App\Models\Classrm;
use App\Models\Mark1;
use App\Models\Mark2;
use App\Models\Student;
use App\Models\Subjects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class Mark1Controller extends Controller
{

    public function index()
    {
        $classrms = Classrm::with('course')->orderBy('course_id', 'desc')->paginate(20)->withQueryString();
        return view('mark1.index', compact('classrms'));
    }


    public function store(Request $request)
    {
        $mark1 = new Mark1();
        if ($request->point > 5) {
            if ($request->confirm_flag == 1) {
                $mark1->status = 1;
            } else {
                $mark1->status = 0;
                $mark2 = new Mark2();
                $mark2->student_id = $request->student_id;
                $mark2->subject_id = $request->subject_id;
                $mark2->class_id = $request->id;
                $mark2->save();
            }
        } else {
            $mark1->status = 0;
            $mark2 = new Mark2();
            $mark2->student_id = $request->student_id;
            $mark2->subject_id = $request->subject_id;
            $mark2->class_id = $request->id;
            $mark2->save();
        }
        $mark1->point = $request->point;
        $mark1->student_id = $request->student_id;
        $mark1->subject_id = $request->subject_id;
        $mark1->class_id = $request->id;
        $mark1->confirm_flag = $request->confirm_flag;
        $mark1->save();
        $classrm = Classrm::find($request->id);
        $classrm->total_learn = Mark1::where('status', 0)->get()->count();
        $classrm->update();
        return redirect()->back()->with('success', 'Thêm điểm thành công!');
    }

    public function show($id)
    {
        $classrm = Classrm::find($id);
        $students = Student::where('class_id', $id)->orderBy('name', 'ASC')->get();
        $subjectss = Subjects::orderBy('name', 'ASC')->get();
        $mark1s = Mark1::with('student', 'subjects')->where('class_id', $id)->orderBy('id', 'desc')->paginate(20)->withQueryString();
        return view('mark1.show', compact('mark1s', 'classrm', 'students', 'subjectss'));
    }

    public function edit($id)
    {
        $mark1 = Mark1::find($id);
        return view('mark1.edit', compact('mark1'));
    }

    public function update(Request $request)
    {
        $mark1 = Mark1::find($request->id);
        $mark2 = Mark2::where('student_id', $mark1->student_id)->where('subject_id', $mark1->subject_id)->first();
        if ($request->point > 5) {
            if ($request->confirm_flag == 1) {
                $mark1->status = 1;
                if ($mark2 !== null) {
                    $mark2->delete();
                }
            } else {
                $mark1->status = 0;
                if ($mark2 == null) {
                    $mark2 = new Mark2();
                    $mark2->student_id = $mark1->student_id;
                    $mark2->subject_id = $mark1->subject_id;
                    $mark2->class_id = $mark1->class_id;
                    $mark2->save();
                }
            }
        } else {
            $mark1->status = 0;
            if ($mark2 == null) {
                $mark2 = new Mark2();
                $mark2->student_id = $mark1->student_id;
                $mark2->subject_id = $mark1->subject_id;
                $mark2->class_id = $mark1->class_id;
                $mark2->save();
            }
        }
        $mark1->point = $request->point;
        $mark1->confirm_flag = $request->confirm_flag;
        $mark1->save();
        $classrm = Classrm::find($mark1->class_id);
        $classrm->total_learn = Mark1::where('status', 0)->get()->count();
        $classrm->update();
        return Redirect::route('mark1-show', $mark1->class_id)->with('success', 'Cập nhật thành công!');
    }

    public function destroy(Mark1 $mark1)
    {
        //
    }
}
