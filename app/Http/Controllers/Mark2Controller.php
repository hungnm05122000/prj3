<?php

namespace App\Http\Controllers;

use App\Models\Classrm;
use App\Models\Learnag;
use App\Models\Mark2;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class Mark2Controller extends Controller
{

    public function index()
    {
        $classrms = Classrm::with('course')->orderBy('course_id', 'desc')->paginate(20)->withQueryString();
        return view('mark2.index', compact('classrms'));
    }


    public function show($id)
    {
        $classrm = Classrm::find($id);
        $mark2s = Mark2::with('student', 'subjects')->where('class_id', $id)->orderBy('id', 'desc')->paginate(20)->withQueryString();
        return view('mark2.show', compact('mark2s', 'classrm'));
    }


    public function edit($id)
    {
        $mark2 = Mark2::find($id);
        return view('mark2.edit', compact('mark2'));
    }


    public function update(Request $request)
    {
        $mark2 = Mark2::find($request->id);
        $learnag = Learnag::where('student_id', $mark2->student_id)->where('subject_id', $mark2->subject_id)->first();
        if ($request->point > 5) {
            if ($request->confirm_flag == 1) {
                $mark2->status = 1;
                if ($learnag !== null) {
                    $learnag->delete();
                }
            } else {
                $mark2->status = 0;
                if ($learnag == null) {
                    $learnag = new Learnag();
                    $learnag->student_id = $mark2->student_id;
                    $learnag->subject_id = $mark2->subject_id;
                    $learnag->class_id = $mark2->class_id;
                    $learnag->save();
                }
            }
        } else {
            $mark2->status = 0;
            if ($learnag == null) {
                $learnag = new Learnag();
                $learnag->student_id = $mark2->student_id;
                $learnag->subject_id = $mark2->subject_id;
                $learnag->class_id = $mark2->class_id;
                $learnag->save();
            }
        }
        $mark2->point = $request->point;
        $mark2->confirm_flag = $request->confirm_flag;
        $mark2->save();
        $classrm = Classrm::find($mark2->class_id);
        $classrm->total_exam = Mark2::where('status', 0)->get()->count();
        $classrm->update();
        return Redirect::route('mark2-show', $mark2->class_id)->with('success', 'Cập nhật thành công!');
    }
}
