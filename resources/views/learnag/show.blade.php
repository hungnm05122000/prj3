@extends('layout.main')

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if (Session::has('success'))
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <strong>{{ Session::get('success') }}</strong>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            @if (Session::has('error'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <strong>{{ Session::get('error') }}</strong>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            <h4 class="card-title">Danh sách học lại ({{ $classrm->name }})</h4>
                            <div class="table-responsive pt-3">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <th>Tên</th>
                                            <th>Môn</th>
                                            <th>Trạng Thái</th>
                                            <th>Hành động</th>
                                        </tr>
                                        @foreach ($learnags as $learnag)
                                            <tr>
                                                <td>
                                                    {{ $learnag->student->name }}
                                                </td>
                                                <td>
                                                    {{ $learnag->subjects->name }}
                                                </td>
                                                <td>
                                                    @if ($learnag->status == 1)
                                                        <span style="color: green; font-weight: bold">
                                                            Qua môn
                                                        </span>
                                                    @else
                                                        <span style="color: red; font-weight: bold">
                                                            Học lại
                                                        </span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a class="btn btn-info" href="{{ route('learnmk', $learnag->id) }}">
                                                        <i class="icon-upload"></i>
                                                        Cập nhật điểm
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pagination">
                {{ $learnags->links() }}
            </div>
        </div>
    </div>
@endsection
