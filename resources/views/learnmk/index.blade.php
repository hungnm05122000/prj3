@extends('layout.main')

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if (Session::has('success'))
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <strong>{{ Session::get('success') }}</strong>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            @if (Session::has('error'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <strong>{{ Session::get('error') }}</strong>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            <h4 class="card-title">
                                Danh sách điểm học lại ({{ $learnag->student->name }}) ({{ $learnag->subjects->name }})
                            </h4>
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalCreate">
                                <i class="icon-upload"></i>
                                <code class="text-white">Thêm Điểm</code>
                            </button>
                            <div class="table-responsive pt-3">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <th>Điểm</th>
                                            <th>Điều kiện</th>
                                        </tr>
                                        @foreach ($learnmks as $learnmk)
                                            <tr>
                                                <td>
                                                    {{ $learnmk->point }}
                                                </td>
                                                <td>
                                                    @if ($learnmk->confirm_flag == 1)
                                                        <span style="color: green; font-weight: bold">
                                                            Đã đóng tiền
                                                        </span>
                                                    @else
                                                        <span style="color: red; font-weight: bold">
                                                            Chưa đóng tiền
                                                        </span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- modalCreate --}}
    <div class="modal fade" id="modalCreate" tabindex="-1" aria-labelledby="CreatePostModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="form-create" action="{{ url('/learnmk') }}" method="POST" style="overflow-y: auto"
                    enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" value="{{ $learnag->id }}">
                    <div class="modal-header">
                        <h4>Manage</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <div>
                                <label for="title-create" class="col-form-label">
                                    <b>Điều kiện: <span style="color: red">*</span></b>
                                </label>
                                <select class="form-control form-control-lg" id="exampleFormControlSelect2"
                                    name="confirm_flag">
                                    <option disabled selected>Chọn điều kiện</option>
                                    <option value="1">Đã đóng tiền</option>
                                    <option value="0">Chưa đóng tiền</option>

                                </select>
                            </div>
                            <div>
                                <label for="title-create" class="col-form-label">
                                    <b>Điểm: <span style="color: red">*</span></b>
                                </label>
                                <input type="text" class="form-control" name="point" placeholder="Thêm điểm" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Thoát</button>
                        <div>
                            <button type="submit" class="btn btn-info">Chấp nhận</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
