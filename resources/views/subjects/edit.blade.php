@extends('layout.main')

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Default form</h4>
                            <p class="card-description">
                                Sửa Môn Học
                            </p>
                            <form class="forms-sample" action="{{ url('/subjects') }}" method="POST">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="id" value="{{ $subjects->id }}">
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Tên ngành học:</label>
                                    <select class="form-control form-control-lg" id="exampleFormControlSelect2"
                                        name="major_id">
                                        @foreach ($majorss as $majors)
                                            <option value="{{ $majors->id }}"
                                                @if ($majors->id == $subjects->major_id) {{ 'selected' }} @endif>
                                                {{ $majors->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Tên môn học:</label>
                                    <input type="text" name="name" class="form-control" id="exampleInputUsername1"
                                        placeholder="Sửa tên" value="{{ $subjects->name }}" required>
                                </div>
                                <button class="btn btn-info btn-icon-text" type="submit">
                                    <i class="mdi mdi-file-check btn-icon-prepend"></i>
                                    Chấp Nhận
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
