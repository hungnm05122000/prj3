<nav class="sidebar sidebar-offcanvas bg-info" id="sidebar">
    <div class="user-profile">
        <div class="user-image">
            <p style="font-size: 22px">
                <i class="mdi mdi-home-outline"></i>
            </p>
        </div>
        <div class="user-name">
            {{ Auth::guard('teacher')->user()->name }}
        </div>
        <div class="user-designation">
            {{ Auth::guard('teacher')->user()->capdo }}
        </div>
    </div>
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="{{ url('/course') }}">
                <i class="icon-box menu-icon"></i>
                <span class="menu-title">Khóa Học</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('/class') }}">
                <i class="icon-command menu-icon"></i>
                <span class="menu-title">Lớp Học</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ Url('/student') }}">
                <i class="icon-head menu-icon"></i>
                <span class="menu-title">Sinh Viên</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('/majors') }}">
                <i class="icon-book menu-icon"></i>
                <span class="menu-title">Ngành Học</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('subjects') }}">
                <i class="icon-file menu-icon"></i>
                <span class="menu-title">Môn Học</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false"
                aria-controls="ui-basic">
                <i class="icon-disc menu-icon"></i>
                <span class="menu-title">Điểm thi</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/mark1') }}">
                            Lần 1
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/mark2') }}">
                            Lần 2
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('/learnag') }}">
                <i class="icon-pie-graph menu-icon"></i>
                <span class="menu-title">Học Lại</span>
            </a>
        </li>
        @if (Auth::guard('teacher')->user()->confirm_flag == 1)
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/teacher') }}">
                    <i class="icon-head menu-icon"></i>
                    <span class="menu-title">Giáo Vụ</span>
                </a>
            </li>
        @endif
    </ul>
</nav>
