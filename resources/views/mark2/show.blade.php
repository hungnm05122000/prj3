@extends('layout.main')

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if (Session::has('success'))
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <strong>{{ Session::get('success') }}</strong>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            @if (Session::has('error'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <strong>{{ Session::get('error') }}</strong>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            <h4 class="card-title">Bảng điểm ({{ $classrm->name }})</h4>
                            <div class="table-responsive pt-3">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <th>Tên</th>
                                            <th>Điểm</th>
                                            <th>Môn</th>
                                            <th>Điều Kiện</th>
                                            <th>Trạng Thái</th>
                                            <th>Ngày</th>
                                            <th>Cập nhật</th>
                                            <th>Sửa</th>
                                        </tr>
                                        @foreach ($mark2s as $mark2)
                                            <tr>
                                                <td>
                                                    {{ $mark2->student->name }}
                                                </td>
                                                <td>
                                                    @if ($mark2->point == null)
                                                        <span style="color: gray; font-weight: bold">
                                                            Đang cập nhật
                                                        </span>
                                                    @else
                                                        {{ $mark2->point }}
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ $mark2->subjects->name }}
                                                </td>
                                                <td>
                                                    @if ($mark2->confirm_flag == 1)
                                                        <span style="color: green; font-weight: bold">
                                                            Đã đóng tiền
                                                        </span>
                                                    @elseif ($mark2->confirm_flag == 3)
                                                        <span style="color: gray; font-weight: bold">
                                                            Đang cập nhật
                                                        </span>
                                                    @else
                                                        <span style="color: red; font-weight: bold">
                                                            Chưa đóng tiền
                                                        </span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($mark2->status == 1)
                                                        <span style="color: green; font-weight: bold">
                                                            Qua môn
                                                        </span>
                                                    @elseif ($mark2->status == 3)
                                                        <span style="color: gray; font-weight: bold">
                                                            Đang cập nhật
                                                        </span>
                                                    @else
                                                        <span style="color: red; font-weight: bold">
                                                            Học lại
                                                        </span>
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ date('d-m-Y H:i', strtotime($mark2->created_at)) }}
                                                </td>
                                                <td>
                                                    {{ date('d-m-Y H:i', strtotime($mark2->updated_at)) }}
                                                </td>
                                                <td>
                                                    <a class="btn btn-warning"
                                                        href="{{ route('mark2-edit', $mark2->id) }}">
                                                        <i class="mdi mdi-file-check btn-icon-append"></i>
                                                        Sửa
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pagination">
                {{ $mark2s->links() }}
            </div>
        </div>
    </div>
@endsection
