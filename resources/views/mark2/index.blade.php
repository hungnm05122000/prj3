@extends('layout.main')

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Lớp Học</h4>
                            <div class="table-responsive pt-3">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <th>Tên</th>
                                            <th>Khóa Học</th>
                                            <th>Sinh viên thi lại / các môn</th>
                                            <th>Hành Động</th>
                                        </tr>
                                        @foreach ($classrms as $classrm)
                                            <tr>
                                                <td>
                                                    {{ $classrm->name }}
                                                </td>
                                                <td>
                                                    {{ $classrm->course->name }}
                                                </td>
                                                <td>
                                                    {{ $classrm->total_learn }}
                                                </td>
                                                <td>
                                                    <a class="btn btn-info"
                                                        href="{{ route('mark2-show', $classrm->id) }}">
                                                        <i class="icon-upload"></i>
                                                        Nhập
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pagination">
                {{ $classrms->links() }}
            </div>
        </div>
    </div>
@endsection
