@extends('layout.main')

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Default form</h4>
                            <p class="card-description">
                                Sửa Giáo Vụ
                            </p>
                            @if (Session::has('success'))
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <strong>{{ Session::get('success') }}</strong>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            @if (Session::has('error'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <strong>{{ Session::get('error') }}</strong>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            <form class="forms-sample" action="{{ url('/teacher') }}" method="POST">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="id" value="{{ $teacher->id }}">
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Tên giáo vụ:</label>
                                    <input type="text" name="name" class="form-control" id="exampleInputUsername1"
                                        placeholder="Sửa tên" value="{{ $teacher->name }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Email:</label>
                                    <input type="text" class="form-control" id="exampleInputUsername1"
                                        placeholder="Sửa tên" value="{{ $teacher->email }}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Cập nhật mật khẩu:</label>
                                    <input type="password" name="password" class="form-control" id="exampleInputUsername1"
                                        placeholder="Mật khẩu">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Cấp độ:</label>
                                    <select class="form-control form-control-lg" id="exampleFormControlSelect2"
                                        name="confirm_flag">
                                        <option value="0" @if ($teacher->confirm_flag == 0) {{ 'selected' }} @endif>
                                            Admin
                                        </option>
                                        <option value="1" @if ($teacher->confirm_flag == 1) {{ 'selected' }} @endif>
                                            Super Admin
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Số điện thoại:</label>
                                    <input type="text" name="phone" class="form-control" id="exampleInputUsername1"
                                        placeholder="Sửa số điện thoại" value="{{ $teacher->phone }}" required>
                                </div>
                                <button class="btn btn-info btn-icon-text" type="submit">
                                    <i class="mdi mdi-file-check btn-icon-prepend"></i>
                                    Chấp Nhận
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
