<?php

use App\Http\Controllers\AuthenticateController;
use App\Http\Controllers\ClassrmController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\LearnagController;
use App\Http\Controllers\LearnmkController;
use App\Http\Controllers\MajorsController;
use App\Http\Controllers\Mark1Controller;
use App\Http\Controllers\Mark2Controller;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\SubjectsController;
use App\Http\Controllers\TeacherController;
use App\Http\Middleware\CheckLogin;
use App\Http\Middleware\CheckLogout;
use Illuminate\Support\Facades\Route;

Route::middleware([CheckLogin::class])->group(function () {
    // welcome
    Route::get('/', function () {
        return view('index');
    });
    // course
    Route::get('/course', [CourseController::class, 'index']);
    Route::post('/course', [CourseController::class, 'store']);
    Route::get('/course/{id}', [CourseController::class, 'edit'])->name('course-edit');
    Route::put('/course', [CourseController::class, 'update']);
    // class
    Route::get('/class', [ClassrmController::class, 'index']);
    Route::post('/class', [ClassrmController::class, 'store']);
    Route::get('/class-edit/{id}', [ClassrmController::class, 'edit'])->name('class-edit');
    Route::put('/class', [ClassrmController::class, 'update']);
    // student
    Route::get('/student', [StudentController::class, 'index']);
    Route::post('/student', [StudentController::class, 'store']);
    Route::get('/student-edit/{id}', [StudentController::class, 'edit'])->name('student-edit');
    Route::put('/student', [StudentController::class, 'update']);
    // majors
    Route::get('/majors', [MajorsController::class, 'index']);
    Route::post('/majors', [MajorsController::class, 'store']);
    Route::get('/majors-edit/{id}', [MajorsController::class, 'edit'])->name('majors-edit');
    Route::put('/majors', [MajorsController::class, 'update']);
    // subjects
    Route::get('/subjects', [SubjectsController::class, 'index']);
    Route::post('/subjects', [SubjectsController::class, 'store']);
    Route::get('/subjects-edit/{id}', [SubjectsController::class, 'edit'])->name('subjects-edit');
    Route::put('/subjects', [SubjectsController::class, 'update']);
    // mark1
    Route::get('/mark1', [Mark1Controller::class, 'index']);
    Route::get('/mark1-show/{id}', [Mark1Controller::class, 'show'])->name('mark1-show');
    Route::post('/mark1', [Mark1Controller::class, 'store']);
    Route::get('/mark1-edit/{id}', [Mark1Controller::class, 'edit'])->name('mark1-edit');
    Route::put('/mark1', [Mark1Controller::class, 'update']);
    // mark2
    Route::get('/mark2', [Mark2Controller::class, 'index']);
    Route::get('/mark2-show/{id}', [Mark2Controller::class, 'show'])->name('mark2-show');
    Route::get('/mark2-edit/{id}', [Mark2Controller::class, 'edit'])->name('mark2-edit');
    Route::put('/mark2', [Mark2Controller::class, 'update']);
    // learnag
    Route::get('/learnag', [LearnagController::class, 'index']);
    Route::get('/learnag-show/{id}', [LearnagController::class, 'show'])->name('learnag-show');
    // learnmk
    Route::get('/learnmk/{id}', [LearnmkController::class, 'index'])->name('learnmk');
    Route::post('/learnmk', [LearnmkController::class, 'store']);
    // teacher
    Route::get('/teacher', [TeacherController::class, 'index']);
    Route::get('/teacher-create', [TeacherController::class, 'create']);
    Route::post('/teacher', [TeacherController::class, 'store']);
    Route::get('/teacher-edit/{id}', [TeacherController::class, 'edit'])->name('teacher-edit');
    Route::put('/teacher', [TeacherController::class, 'update']);
    Route::delete('/teacher', [TeacherController::class, 'destroy']);
    // logout
    Route::get('/logout', [AuthenticateController::class, 'logout']);
});

Route::middleware([CheckLogout::class])->group(function () {
    // login
    Route::get('/login', [AuthenticateController::class, 'login']);
    Route::post('/login', [AuthenticateController::class, 'signin']);
});
