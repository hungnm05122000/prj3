<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('learnmks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('point');
            $table->unsignedInteger('learnag_id');
            $table->foreign('learnag_id')->references('id')->on('learnags');
            $table->string('confirm_flag');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('learnmks');
    }
};
